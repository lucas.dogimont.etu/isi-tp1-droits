# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: DOGIMONT, Lucas, lucas.dogimont.etu@univ-lille.fr

- Nom, Prénom, email: GENART, Valentin, valentin.genart.etu@univ-lille.fr

## Question 1

Le processus ne peut pas écrire dans le fichier car l'utilisateur toto n'a pas les droits en écriture sur le fichier.

## Question 2

x pour un repertoire signifie qu'il est possible de naviguer dans ce répertoire, d'entrer dans sa hiérarchie.  

L'utilisateur toto ne peut pas accéder au dossier mydir, car le groupe auquel il appartient n'a pas les droits d'execution dessus et toto n'est pas propriétaire du fichier.

toto peut lister les éléments de mydir, mais ne peut pas voir leurs propriétés. Cela s'explique par le fait que le groupe auquel toto appartient n'a pas les droits d'execution sur le dossier, mais a les droits de lecture.

## Question 3

1000 = ubuntu
1001 = toto

Pour toto:  
* EUID et RUID : 1001  
* EGID et RGID : 1000
Le processus n'arrive pas à ouvrir le fichier en lecture

Avec le flag set-user-id:
* EUID, EGID et RGID: 1000
* RUID: 1001
Le processus peut ouvrir le fichier en lecture.

Note : set_user_id permet au fichier executable de s'octroyer les droits du propriétaire pour réaliser temporairement toutes les opérations que le fichier comporte. Sans le set_user_id, le fichier executerait ce qu'il contient avec les droits de l'utilisateur. Par conséquent, il ne pourrait pas tout executer.

## Question 4

Avec le flag set-user-id et l'utilisateur toto:
* EUID: 1001
* EGID: 1000

Un utilisateur peut changer un attribut du fichier /etc/passwd sans demander à l'administrateur en passant par un programme qui le fait à sa place et ayant le flag set-user-id

## Question 5

La commande `chfn` permet de modifier les informations d'un utilisateur dans le fichiet /etc/passwd  

	toto@vm1:/home/ubuntu$ ls -al /usr/bin/chfn  
	-rwsr-xr-x 1 root root 85064 May 28 2020 /usr/bin/chfn

Ce fichier appartient à l'administrateur qui y a tout les droits. Les autres utilisateurs peuvent le lire et l'exécuter, mais pas le modifier. Il porte le flag set-user-id, donc n'importe quel utilisateur qui l'exécute bénéficiera d'accès privilégiés pour opérer sur les fichiers qu'il utilise.

Les informations du fichier /etc/passwd sont bien mises à jour lors de l'exécution de chfn par toto.

## Question 6

Les mots de passe sont stockés hashés dans le fichier /etc/shadow qui n'est lisible que par root. Cela évite de les stocker dans /etc/passwd qui est accessible à tous le monde.

## Question 7

Il existe plusieurs solutions. L'une d'entres-elles est la suivante : 

Créer 3 groupes et 3 utilisateurs : 
- group_a : lambda_a, admin 
- group_b : lambda_b, admin 
- group_ab : lambda_a, lambda_b, admin 


```
ls -al
drwxrws--T  2 admin    group_a  4096 Jan 20 15:58 dir_a
drwxrws--T  2 admin    group_b  4096 Jan 20 14:39 dir_b
drwxr-x--T  2 admin    group_ab 4096 Jan 20 16:07 dir_c
```


## Question 8

Le programme et les scripts dans le repertoire *question8*.
`make` doit être lancé avec root afin que les bons droits soient accordés à l'exécutable.

## Question 9

Le programme et les scripts dans le repertoire *question9*.
`make` doit être lancé avec root afin que les bons droits soient accordés à l'exécutable.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








