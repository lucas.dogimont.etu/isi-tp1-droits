#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <crypt.h>

int main(int args, char** argv) {
	if(args > 1) {
		perror("Vous avez entré trop d'arguments.\n");
		exit(EXIT_FAILURE);
	}
	FILE* fp;
	FILE* fnew;
	fp = fopen("/home/admin/passwd", "r");
	fnew = fopen("/home/admin/passwdnew", "w+");
	if(!fp){
		perror("Erreur lors de l'ouverture du fichier passwd.\n");
		exit(EXIT_FAILURE);
	}
	if(!fnew) {
		perror("Erreur lors de la création du fichier passwdnew.\n");
		exit(EXIT_FAILURE);
	}
	char ligne[150];
	char idUserChar[10];
	sprintf(idUserChar, "%d", getuid());
	int boolean = 0;
	char* salt = "$1$aK6i0f.Y";
	while(fgets(ligne, 150, fp)) {
		if(strstr(ligne, idUserChar)) {
			printf("Saisissez votre mot de passe :\n");
			char oldmdp[40] = {0};
			scanf("%s", oldmdp);
			if(strstr(ligne, crypt(oldmdp, salt))) {
				printf("Saisissez votre nouveau mot de passe :\n");
				char newmdp[40] = {0};
				scanf("%s", newmdp);
				fprintf(fnew, "%s %s\n", idUserChar, crypt(newmdp, salt));
				boolean = 1;
			} else {
				printf("Mauvais mot de passe.\n");
				remove("/home/admin/passwdnew");
				fclose(fp);
				fclose(fnew);
				exit(EXIT_FAILURE);
			}
		} else {
			fprintf(fnew, "%s", ligne);
		}
	}
	//Si l'utilisateur n'a jamais eu de mot de passe (donc pas dans passwd)
	if(boolean == 0) {
		printf("Saisissez votre nouveau mot de passe :\n");
		char mdpnew[40] = {0};
		scanf("%s", mdpnew);
		fprintf(fnew, "%s %s\n", idUserChar, crypt(mdpnew, salt));
	}
	remove("/home/admin/passwd");
	rename("/home/admin/passwdnew", "/home/admin/passwd");
	fclose(fnew);
	fclose(fp);
	return 0;
}
