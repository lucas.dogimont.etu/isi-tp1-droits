#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <crypt.h>

int main(int args, char** argv) {
	if(args < 2) {
		perror("Veuillez entrer le nom du fichier.\n");
		exit(EXIT_FAILURE);
	}
	if(args > 2) {
		perror("Vous avez entré trop d'arguments.\n");
		exit(EXIT_FAILURE);
	}
	if(access(argv[1], F_OK) != 0) {
		perror("Veuillez entrer le nom d'un fichier existant.\n");
		exit(EXIT_FAILURE);
	}
	
	struct stat fichier;
	if(stat(argv[1], &fichier) == -1) {
		perror("Erreur lors de la récupération des données du fichier.\n");
		exit(EXIT_FAILURE);
	}
	int gidUser = getgid();
	int gidFile = fichier.st_gid;
	if(gidUser != gidFile) {
		perror("Vous ne pouvez pas supprimer ce fichier.\n");
		exit(EXIT_FAILURE);
	}
	printf("Saisissez votre mot de passe :\n");
	char mdp[40] = {0};
	scanf("%s", mdp);
	FILE* fp;
	fp = fopen("/home/admin/passwd", "r");
	if(!fp) {
		perror("Erreur lors de l'ouverture du fichier passwd\n");
		exit(EXIT_FAILURE);
	}
	char ligne[150];
	int idUser = getuid();
	char idUserChar[150];
	sprintf(idUserChar, "%d", idUser);
	char* salt = "$1$aK6i0f.Y";
	while(fgets(ligne, 150, fp)) {
		if(strstr(ligne, idUserChar) && strstr(ligne, crypt(mdp,salt))) {
			remove(argv[1]);
			fclose(fp);
			printf("Le fichier %s a bien été supprimé.\n", argv[1]);
			exit(EXIT_SUCCESS);
		}
	}
	fclose(fp);
	perror("Votre mot de passe est incorrect.\n");
	return 0;
}

