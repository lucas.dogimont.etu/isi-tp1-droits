#!/bin/bash

echo "cd dir_b"
cd dir_b
ls -l
echo "User of groupe_b can access dir_b"
echo "------------------------------------------------------"

echo "echo \"blabla\" >> file_b"
echo "blabla" >> file_b
echo "User of groupe_b can modify a file of dir_b that belong to him"
echo "------------------------------------------------------"

echo "echo \"blabla\" >> file_bb"
echo "blabla" >> file_bb
echo "User of groupe_b can modify a file of dir_b that do not belong to him"
echo "------------------------------------------------------"

echo "mv file_b file_b2"
mv file_b file_b2
ls -l
echo "User of groupe_b can rename or remove a file of dir_b that belong to him"
echo "------------------------------------------------------"

echo "mv file_bb file_bb2"
mv file_bb file_bb2
ls -l
echo "User of groupe_b cannot rename or remove a file of dir_b that do not belong to him"
echo "------------------------------------------------------"

echo "touch file_b3"
touch file_b3
ls -l
echo "User of groupe_b can create new file in dir_b"
echo "------------------------------------------------------"

echo "cd ../dir_c"
cd ../dir_c
ls -l
echo "User of groupe_b can access dir_c"
echo "------------------------------------------------------"

echo "cat file_c"
cat file_c
echo "User of groupe_b can read files of dir_c"
echo "------------------------------------------------------"

echo "echo \"blabla\" >> file_c"
echo "blabla" >> file_c
echo "User of groupe_b cannot modify a file of dir_c"
echo "------------------------------------------------------"

echo "touch file_c2"
touch file_c2
echo "User of groupe_b cannot create a file in dir_c"
echo "------------------------------------------------------"

echo "mv file_c file_c2"
mv file_c file_c2
ls -l
echo "User of groupe_b cannot rename or remove a file of dir_c"
echo "------------------------------------------------------"

echo "cd ../dir_a"
cd ..
cd dir_a
echo "User of groupe_b cannot access dir_a"
echo "------------------------------------------------------"

mv dir_b/file_b2 dir_b/file_b 1>/dev/null 2>&1
rm dir_b/file_b3 1>/dev/null 2>&1

