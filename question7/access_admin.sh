#!/bin/bash

echo "cd dir_a"
cd dir_a
ls -l
echo "User admin can access dir_a"
echo "------------------------------------------------------"

echo "echo \"blabla\" >> file_a"
echo "blabla" >> file_a
echo "User admin can modify files of dir_a"
echo "------------------------------------------------------"

echo "mv file_a file_a2"
mv file_a file_a2
ls -l
echo "User admin can rename or remove a file of dir_a that belong to him"
echo "------------------------------------------------------"

echo "mv file_aa file_aa2"
mv file_aa file_aa2
ls -l
echo "User admin can rename or remove a file of dir_a that do not belong to him"
echo "------------------------------------------------------"

echo "touch file_a3"
touch file_a3
ls -l
echo "User admin can create new file in dir_a"
echo "------------------------------------------------------"

echo "cd ../dir_c"
cd ../dir_c
ls -l
echo "User admin can access dir_c"
echo "------------------------------------------------------"

echo "cat file_c"
cat file_c
echo "User admin can read files of dir_c"
echo "------------------------------------------------------"

echo "echo \"blabla\" >> file_c"
echo "blabla" >> file_c
echo "User admin can modify a file of dir_c"
echo "------------------------------------------------------"

echo "touch file_c2"
touch file_c2
echo "User admin can create a file in dir_c"
echo "------------------------------------------------------"

echo "mv file_c file_c2"
mv file_c file_c2
ls -l
echo "User admin can rename or remove a file of dir_c"
echo "------------------------------------------------------"

echo "cd ../dir_b"
cd ..
cd dir_b
echo "User admin can access dir_b"
echo "------------------------------------------------------"

cd ..
mv dir_c/file_c2 dir_c/file_c 1>/dev/null 2>&1
mv dir_a/file_a2 dir_a/file_a 1>/dev/null 2>&1
mv dir_a/file_aa2 dir_a/file_aa 1>/dev/null 2>&1
rm dir_a/file_a3 1>/dev/null 2>&1

